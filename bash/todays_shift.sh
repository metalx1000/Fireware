#!/bin/bash
###################################################################### 
#Copyright (C) 2020  Kris Occhipinti
#https://filmsbykris.com

#Calculate which shift is today (based on a 3 day cycle)

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

A="$(date -d 2013-05-01 +%s)"
B="$(date -d 2013-05-02 +%s)"
C="$(date -d 2013-05-03 +%s)"

A=$(( ( `date +%s` - $A ) / (24*3600)%3 ))
B=$(( ( `date +%s` - $B ) / (24*3600)%3 ))
C=$(( ( `date +%s` - $C ) / (24*3600)%3 ))

[[ "$A" == 0 ]] && echo "Today is A Shift"
[[ "$B" == 0 ]] && echo "Today is B Shift"
[[ "$C" == 0 ]] && echo "Today is C Shift"
